#include <cstdint>
#include <cstdio>

#define CH(x, y, z) ((x & y) ^ ((~x) & z))
#define MAJ(x, y, z) ((x & y) ^ (x & z) ^ (y & z))
#define BIG_SIGMA_0(x) (right_rotate_bits(x, 28) ^ right_rotate_bits(x, 34) ^ right_rotate_bits(x, 39))
#define BIG_SIGMA_1(x) (right_rotate_bits(x, 14) ^ right_rotate_bits(x, 18) ^ right_rotate_bits(x, 41))
#define SMALL_SIGMA_0(x) (right_rotate_bits(x, 1) ^ right_rotate_bits(x, 8) ^ (x >> 7))
#define SMALL_SIGMA_1(x) (right_rotate_bits(x, 19) ^ right_rotate_bits(x, 61) ^ (x >> 6))

uint64_t constants_k[80] = {
    0x428a2f98d728ae22, 0x7137449123ef65cd, 0xb5c0fbcfec4d3b2f, 0xe9b5dba58189dbbc, 0x3956c25bf348b538,
    0x59f111f1b605d019, 0x923f82a4af194f9b, 0xab1c5ed5da6d8118, 0xd807aa98a3030242, 0x12835b0145706fbe,
    0x243185be4ee4b28c, 0x550c7dc3d5ffb4e2, 0x72be5d74f27b896f, 0x80deb1fe3b1696b1, 0x9bdc06a725c71235,
    0xc19bf174cf692694, 0xe49b69c19ef14ad2, 0xefbe4786384f25e3, 0x0fc19dc68b8cd5b5, 0x240ca1cc77ac9c65,
    0x2de92c6f592b0275, 0x4a7484aa6ea6e483, 0x5cb0a9dcbd41fbd4, 0x76f988da831153b5, 0x983e5152ee66dfab,
    0xa831c66d2db43210, 0xb00327c898fb213f, 0xbf597fc7beef0ee4, 0xc6e00bf33da88fc2, 0xd5a79147930aa725,
    0x06ca6351e003826f, 0x142929670a0e6e70, 0x27b70a8546d22ffc, 0x2e1b21385c26c926, 0x4d2c6dfc5ac42aed,
    0x53380d139d95b3df, 0x650a73548baf63de, 0x766a0abb3c77b2a8, 0x81c2c92e47edaee6, 0x92722c851482353b,
    0xa2bfe8a14cf10364, 0xa81a664bbc423001, 0xc24b8b70d0f89791, 0xc76c51a30654be30, 0xd192e819d6ef5218,
    0xd69906245565a910, 0xf40e35855771202a, 0x106aa07032bbd1b8, 0x19a4c116b8d2d0c8, 0x1e376c085141ab53,
    0x2748774cdf8eeb99, 0x34b0bcb5e19b48a8, 0x391c0cb3c5c95a63, 0x4ed8aa4ae3418acb, 0x5b9cca4f7763e373,
    0x682e6ff3d6b2b8a3, 0x748f82ee5defb2fc, 0x78a5636f43172f60, 0x84c87814a1f0ab72, 0x8cc702081a6439ec,
    0x90befffa23631e28, 0xa4506cebde82bde9, 0xbef9a3f7b2c67915, 0xc67178f2e372532b, 0xca273eceea26619c,
    0xd186b8c721c0c207, 0xeada7dd6cde0eb1e, 0xf57d4f7fee6ed178, 0x06f067aa72176fba, 0x0a637dc5a2c898a6,
    0x113f9804bef90dae, 0x1b710b35131c471b, 0x28db77f523047d84, 0x32caab7b40c72493, 0x3c9ebe0a15c9bebc,
    0x431d67c49c100d4c, 0x4cc5d4becb3e42b6, 0x597f299cfc657e2a, 0x5fcb6fab3ad6faec, 0x6c44198c4a475817
};

char digit2hex[] = "0123456789abcdef";

uint64_t right_rotate_bits(uint64_t num, uint64_t amount) {
    uint64_t right_part;

    right_part = num & ((1ULL << (amount + 1ULL)) - 1ULL);
    num >>= amount;
    num |= (right_part << (64ULL - amount));

    return num;
}

uint64_t get_file_padding(char *filename, uint64_t &nzeros) {
    uint64_t file_size;
    uint64_t mod_1024;
    FILE *fh;

    fh = fopen(filename, "rb");

    if(fh != NULL) {
        fseek(fh, 0, SEEK_END);
        file_size = (uint64_t) ftell(fh);
        fclose(fh);

        mod_1024 = (file_size * 8ULL + 1) % 1024ULL;

        if(mod_1024 < 896ULL) {
            nzeros = 896ULL - mod_1024;
        }
        else if(mod_1024 > 896ULL) {
            nzeros = 896ULL + (1024ULL - mod_1024);
        }
        else {
            nzeros = 0ULL;
        }

        return file_size;
    }

    return -1UL;
}

void compute_message_blocks(uint64_t message_chunks[16], uint64_t (&w)[80]) {
    // Initialize blocks with message
    for(int i = 0; i <= 15; ++i) {
        w[i] = message_chunks[i];
    }

    for(int i = 16; i < 80; ++i) {
        w[i] = SMALL_SIGMA_1(w[i - 2]) + w[i - 7] + SMALL_SIGMA_0(w[i - 15]) + w[i - 16];
        //printf("w iter %d: 0x%llx 0x%llx 0x%llx 0x%llx\n", i, SMALL_SIGMA_1(w[i - 2]), w[i - 7], SMALL_SIGMA_0(w[i - 15]), w[i - 16]);
    }
}

void compute_sha512_iter(uint64_t w[80], uint64_t k[80], uint64_t (&hash_buckets)[8]) {
    uint64_t registers[8];
    uint64_t t1, t2;

    // Initialize registers with hash buckets
    for(int i = 0; i < 8; ++i) {
        registers[i] = hash_buckets[i];
    }

    // Apply compression function
    for(int i = 0; i < 80; ++i) {
        t1 = registers[7] + BIG_SIGMA_1(registers[4]) + CH(registers[4], registers[5], registers[6]) + k[i] + w[i];
        t2 = BIG_SIGMA_0(registers[0]) + MAJ(registers[0], registers[1], registers[2]);

        registers[7] = registers[6];
        registers[6] = registers[5];
        registers[5] = registers[4];
        registers[4] = registers[3] + t1;
        registers[3] = registers[2];
        registers[2] = registers[1];
        registers[1] = registers[0];
        registers[0] = t1 + t2;

        /*
        printf("Iteration %d\n", i);
        printf("t1, 2: 0x%llx 0x%llx\n", t1, t2);

        for(int j = 0; j < 8; ++j) {
            printf("0x%llx ", registers[j]);

            if(j == 3) {
                printf("\n");
            }
        }
        printf("\n");
        */
    }

    // Compute next hash value
    for(int i = 0; i < 8; ++i) {
        hash_buckets[i] += registers[i];
    }
}

void get_sha512(char *filename, char (&hash_result)[130]) {
    FILE *fh;
    bool is_boundary_added, is_done_read;
    uint8_t read_chars[130];
    size_t read_size;
    uint64_t message_buffer[16];
    uint64_t blocks_w[80];
    uint64_t file_size, npad_zeros;

    uint64_t hash_buckets[] = {
        0x6a09e667f3bcc908,
        0xbb67ae8584caa73b,
        0x3c6ef372fe94f82b,
        0xa54ff53a5f1d36f1,
        0x510e527fade682d1,
        0x9b05688c2b3e6c1f,
        0x1f83d9abfb41bd6b,
        0x5be0cd19137e2179
    };

    // Get file size and number of zeros to pad
    file_size = get_file_padding(filename, npad_zeros);

    // Read file as a stream
    fh = fopen(filename, "rb");

    is_done_read = false;
    is_boundary_added = false;

    if(fh != NULL) {
        while(!is_done_read) {
            if(!is_boundary_added) {
                read_size = fread(read_chars, sizeof(char), 128, fh);

                if(read_size < 128) {
                    // We reached the end of file! :)
                    // Append zeros until we fill-in all 112-128 bytes
                    for(int i = read_size; i < 128; ++i) {
                        if(!is_boundary_added) {
                            // Append '1' to the message before the zero pads
                            read_chars[i] = 0x80U;
                            npad_zeros -= 7ULL;

                            is_boundary_added = true;
                        }
                        else {
                            read_chars[i] = 0x0U;
                            npad_zeros -= 8ULL;
                        }

                        if(npad_zeros <= 0ULL) {
                            // No zeros to pad anymore
                            break;
                        }
                    }
                }
            }
            else {
                // Append zeros until we fill-in all 112 bytes
                if(npad_zeros > 0) {
                    for(int i = 0; i < 128; ++i) {
                        read_chars[i] = 0x0U;
                        npad_zeros -= 8ULL;

                        if(npad_zeros <= 0ULL) {
                            // No zeros to pad anymore
                            break;
                        }
                    }
                }
            }

            if(is_boundary_added && npad_zeros <= 0) {
                // Zero-out first half of file length
                for(int i = 112; i < 120; ++i) {
                    read_chars[i] = 0x0U;
                }

                // Last three bits are the first three bits of the file length
                // 1 byte = 8 bits
                read_chars[119] = (file_size >> 61);

                // Append file size
                for(int i = 120; i < 128; ++i) {
                    read_chars[i] = ((file_size << 3U) >> (8U * (7U - (i - 120U)))) & 0xFFU;
                }

                is_done_read = true;
            }

            // Convert char array into uint64_t array
            for(int i = 0; i < 16; ++i) {
                for(int j = 0; j < 8; ++j) {
                    message_buffer[i] <<= 8ULL;
                    message_buffer[i] |= read_chars[8 * i + j];
                }
            }

            /*
            for(int i = 0; i < 16; ++i) {
                printf(">> %llx\n", message_buffer[i]);
            }
            */

            // Compute one round of SHA512
            compute_message_blocks(message_buffer, blocks_w);
            compute_sha512_iter(blocks_w, constants_k, hash_buckets);

            /*
            for(int i = 0; i < 80; ++i) {
                printf("message block %d: 0x%llx\n", i, blocks_w[i]);
            }
            */
        }

        fclose(fh);
    }

    // Convert hash buckets to characters
    for(int i = 0; i < 8; ++i) {
        for(int j = 0; j < 16; ++j) {
            int nibble = (hash_buckets[i] >> (60 - (j << 2))) & 0xF;

            hash_result[(i << 4) + j] = digit2hex[nibble];
        }
    }

    hash_result[128] = 0;
}

int main() {
    char file_name[] = "/Users/ucl/Desktop/nchan_words.xcf";
    char hash_result[130];
    uint64_t a, b;

    get_sha512(file_name, hash_result);

    printf("%s  %s\n", hash_result, file_name);

    return 0;
}